#include <SomfyRTSRemoteControl.h>

SomfyRTSRemoteControl* SomfyRTSRemoteControl::CreateDevice(String name, pin_size_t pin_up, pin_size_t pin_down, pin_size_t pin_stopMy, pin_size_t pin_switch, pin_size_t led_ch1, pin_size_t led_ch2, pin_size_t led_ch3, pin_size_t led_ch4)
{
    SomfyRTSRemoteControl* obj = new SomfyRTSRemoteControl();

    obj->_name = name; 
    obj->_up = pin_up;
    obj->_down = pin_down;
    obj->_stopMy = pin_stopMy;
    obj->_switch = pin_switch;
    obj->_ch1 = led_ch1;
    obj->_ch2 = led_ch2;
    obj->_ch3 = led_ch3;
    obj->_ch4 = led_ch4;
    
    obj->setupOutputPin(pin_up);
    obj->setupOutputPin(pin_down);
    obj->setupOutputPin(pin_stopMy);
    obj->setupOutputPin(pin_switch);

    obj->setupInputPin(led_ch1);
    obj->setupInputPin(led_ch2);
    obj->setupInputPin(led_ch3);
    obj->setupInputPin(led_ch4);

    obj->_timerStatusUpdate.setInterval(UPDATE_INTERVAL);

    // initialize the state and switch to channel 1
    obj->readChannel();
    bool success = obj->switchChannel(obj->_ch1);
    
    if (success)
    {
        DBGLN("SomfyRTSRemoteControl: Failed to change channel");
    }
    return obj;
}

void SomfyRTSRemoteControl::readChannel()
{
    
    /**
     * @brief Technical details
     * 
     * The Somfy remote control will flash the channel LED. They will be on for 70ms and off for 70ms.
     * If in the ALL-mode, all LEDs will flash simultaneously.
     * 
     * Approach is to measure the pin state for a period of approximately 100ms at a frequency of 35ms.
     * Due to overheads, we will wait 30ms between runs. Therefore, the look has three iterations.
     * 
     * If more than one LED is on, we assume to be in ALL-mode.
     */

    pin_size_t old = _curChannel;
    
    bool leds[4] = {false,false,false,false};
    
    // 3 sampling runs
    for (int i = 0; i < 3; i++)
    {
        // sets true if it is ON in any of the sampling runs
        leds[0] |= readLEDState(_ch1);
        leds[1] |= readLEDState(_ch2);
        leds[2] |= readLEDState(_ch3);
        leds[3] |= readLEDState(_ch4);

        delay(30);
    }

    if (leds[0]) _curChannel = _ch1;
    if (leds[1]) _curChannel = _ch2;
    if (leds[2]) _curChannel = _ch3;
    if (leds[3]) _curChannel = _ch4;



    if (leds[0] + leds[1] + leds[2] + leds[3] > 1)
    {
        _curChannel = CHANNEL_ALL;
    }
    
    // print status on change
    if (old != _curChannel)
    {
        DBG("SomfyRTSRemoteControl: Channel Change: ");
        DBGLN(_curChannel);
    }
}

bool SomfyRTSRemoteControl::switchChannel(SomfyChannel channel)
{
    // try to switch the channel at most 5 times since we have a 4 channel remote (4 States + an ALL-mode + 1 to ensure that the LED is on)
    
    for (size_t i = 0; i < 5; i++)
    {
        // switch if incorrect
        if (_curChannel != channel)
        {
            triggerButton(_switch);
            DBGLN("SomfyRTSRemoteControl: Switching Channel");
            readChannel();
        }
    }

    // check if the change has been successful
    return (channel == _curChannel);
    
}

String SomfyRTSRemoteControl::ToString()
{
    String s = "SomfyRTSRemoteControl:\t";
    s += _name;
    s += "\n\t_curChannel:\t";
    s += _curChannel;
    s += "\n\tOutput PINs:\t UP=";
    s += _up;
    s += "\tDOWN=";
    s += _down;
    s += "\tSTOP/MY=";
    s += _stopMy; 
    s += "\tSWITCH=";
    s += _switch;
    s += "\n\tInput PINs:\tCH1=";
    s += _ch1;
    s += "\tCH2=";
    s += _ch2;
    s += "\tCH3=";
    s += _ch3;
    s += "\tCH4=";
    s += _ch4;
    return s;
}

bool SomfyRTSRemoteControl::Up(SomfyChannel channel)
{

    if (switchChannel(channel))
    {
        triggerButton(_up);
        DBGLN("SomfyRTSRemoteControl: Trigger UP");
        return true;
    }
    else
    {  
        DBGLN("SomfyRTSRemoteControl: Switching Channel failed (UP)");
        return false;
    }
}

bool SomfyRTSRemoteControl::Down(SomfyChannel channel)
{
    if (switchChannel(channel))
    {
        triggerButton(_down);
        DBGLN("SomfyRTSRemoteControl: Trigger DOWN");
        return true;
    }
    else
    {  
        DBGLN("SomfyRTSRemoteControl: Switching Channel failed (DOWN)");
        return false;
    }
}


bool SomfyRTSRemoteControl::StopMy(SomfyChannel channel)
{
    if (switchChannel(channel))
    {
        triggerButton(_stopMy);
        DBGLN("SomfyRTSRemoteControl: Trigger STOP/MY");
        return true;
    }
    else
    {
        DBGLN("SomfyRTSRemoteControl: Switching Channel failed (STOP/MY)");
        return false;
    }
    
}

void SomfyRTSRemoteControl::UpdateState()
{
    // once the timer has elapsed we read the current channel
    if (_timerStatusUpdate.isReady())
    {
        triggerButton(_switch);
        readChannel();
        DBG("SomfyRTSRemoteControl: Periodically reading channel settings... ");
        DBGLN(_curChannel);
        
        _timerStatusUpdate.reset();
    }

}

