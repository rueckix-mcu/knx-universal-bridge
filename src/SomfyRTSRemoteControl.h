#pragma once


#include <KNXDeviceInterface.h>
#include <SimpleTimer.h>

typedef pin_size_t SomfyChannel;

class SomfyRTSRemoteControl: public IKNXDeviceInterface
{
private:
    String _name;
    pin_size_t _up;
    pin_size_t _down;
    pin_size_t _stopMy;
    pin_size_t _switch;
    pin_size_t _ch1;
    pin_size_t _ch2;
    pin_size_t _ch3;
    pin_size_t _ch4;
    
    pin_size_t _curChannel; // we are abusing the pin numbers as channel numbers
    
    SimpleTimer _timerStatusUpdate;
    

public:
    static constexpr pin_size_t CHANNEL_ALL = 0; // special case that should not overlap with pin numbers
    static constexpr uint32_t UPDATE_INTERVAL = 60 * 60 * 1000; // Update the channel settings every 1h 

public:

    /**
     * @brief Create a device object representating a 4 channel remote control for Somfy RTS blinds
     * 
     * @param name          - friendly name of the object
     * @param pin_up        - up button (active LOW)
     * @param pin_down      - down button (active LOW)
     * @param pin_stopMy     - stop / my button (active LOW). Behaviour depends on whether the blind is running (-> stop) or at rest (-> my position)
     * @param pin_switch    - channel switch button (active LOW)
     * @param led_ch1       - channel 1 indicator LED (active LOW)
     * @param led_ch2       - channel 2 indicator LED (active LOW) 
     * @param led_ch3       - channel 3 indicator LED (active LOW) 
     * @param led_ch4       - channel 4 indicator LED (active LOW) 
     * @return IKNXDeviceInterface* 
     */
    static SomfyRTSRemoteControl* CreateDevice(String name, pin_size_t pin_up, pin_size_t pin_down, pin_size_t pin_stopMy, pin_size_t pin_switch, pin_size_t led_ch1, pin_size_t led_ch2, pin_size_t led_ch3, pin_size_t led_ch4);


    public:
        virtual String ToString();
        virtual String GetDeviceType() {return "SomfyRTSRemoteControl";};

        /**
         * @brief Updating the state means keeping the current channel up to date after a timer expires
         * 
         */
        virtual void UpdateState();
        
        /**
         * @brief Trigger the Up button
         * 
         * @param channel - target channel
         * @return true  - iff successful
         * @return false 
         */
        bool Up(SomfyChannel channel);
        

        /**
         * @brief Trigger the Down butotn
         * 
         * @param channel - target channel
         * @return true - iff successful
         * @return false 
         */
        bool Down(SomfyChannel channel);


        /**
         * @brief Trigger the Stop/My button
         * 
         * @param channel - target channel
         * @return true - iff successful
         * @return false 
         */
        bool StopMy(SomfyChannel channel);


protected: 

        /**
         * @brief Switch channel to target channel
         * 
         * @param channel - target channel
         * @return true - if successful
         * @return false - if unsuccessful (for probably physical reasons)
         */
        virtual bool switchChannel(SomfyChannel channel);


        /**
         * @brief Determine the current Channel based on LEDs
         * 
         */
        void readChannel();        
        
};
