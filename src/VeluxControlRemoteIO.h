#pragma once


#include <VeluxControl.h>

class VeluxControlRemoteIO: public IVeluxControl
{

    

public:

    /**
     * @brief Create a device object representating a 1 channel remote control for Velux IO
     * 
     * @param name          - friendly name of the object
     * @param pin_open        - up button (active LOW)
     * @param pin_close      - down button (active LOW)
     * @param pin_stop     - stop button (active LOW). 
     * @return VeluxControlRemoteIO* 
     */
    static VeluxControlRemoteIO* CreateDevice(String name, pin_size_t pin_open, pin_size_t pin_close, pin_size_t pin_stop);

    

    public:
        virtual String GetDeviceType() { return "VeluxControlRemoteIO"; };

        /**
         * @brief Trigger the Open button
         * 
         */
        virtual void Open();
        
        /**
         * @brief Trigger the Close butotn
         * 
         */
        virtual void Close();

        /**
         * @brief Trigger the Stop button
         * 
         */
        virtual void Stop();


};
