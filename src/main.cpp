#include <knx.h>

#include <vector>

#include "platform.h"

#include "SomfyBlind.h"
#include "KNXDeviceInterface.h"
#include "VeluxControlRemoteIO.h"
#include "VeluxControlWindowMasterGPIO.h"
#include "VeluxWindow.h"
#include "SomfyBlindAll.h"



/**
 * @brief Total pins
 * 
 * UART                 2
 * VELUX Window #1      3
 * VELUX Window #2      3
 * VELUX Window #3      3
 * VELUX Window #4      3
 * Somfy 4CH            8
 * KNX Prog Btn         0 (on board)
 * KNX Prog LED         0 (on board)
 * Serial (debug)       2
 * ===========================
 *                      24
 * 
 */



/**
 * @brief Power Calculation
 * 
 * All measured at input 5V rail
 * 
 * ## STM32F411
 * 6mA when issuing __wfi in the main loop
 * 
 * 
 * ## 4 CH Somfy Control 
 * Idle: 0 mA
 * Idle with all LEDs on: 7.5mA
 * Sending: 11mA
 * 
 * ## Velux Remote  [x4]
 * Idle: 0 mA
 * Sending: 32mA
 * 
 * ## Total
 * Idle:  6mA
 * Idle when blinking: 13.5mA
 * Sending (max / only one at a time): 45.5 mA
 * 
 * Siemens BCU provides 
 * peak: 50mA@5V
 * Nominal 10mA@5V
 * 
 * Nominally we stay below the 10mA with exceptionally exceeding while switching channels.
 * Sending is a short burst of 100ms and stays below the 50mA peak.
 * 
 * 
 * ATTN: When using VexuControlWindowMasterGPIO, we are controlling a 2 channel relay. Each relay coil takes 65mA. Cannot be powered from BCU.
 * BCU needs to be isolated from the STM via a fast rate coupler, e.g., ADUM 1201
 * 
 * 
 * ATTN: Rain event (Alarm or NoAlarm) must be sent periodically on the bus. There is no automatic dismissal of alarms.
 * 
 */


/**
 * @brief Future extensions
 * 
 * // IDEA: adding Rf-433 interface that can be used instead of a VeluxWindow control (3 pin interface + 2 pin power 3V3)
 * // IDEA: adding a cooldown timer for rain alarm, after which the motors are unlocked until a new alarm is received
 * 
 */


/**
 * @brief Parameters - they are 32 bit, so 4 byte apart
 * 
 * knx.paramInt(0)      - Runtime [s] of Blind 1
 * knx.paramInt(4)      - Runtime [s] of Blind 2
 * knx.paramInt(8)      - Runtime [s] of Blind 3
 * knx.paramInt(12)      - Runtime [s] of Blind 4
 * knx.paramInt(16)      - Runtime [s] of Window (only used for old WindowMater motor driver)
 * knx.paramInt(20)      - Runtime [s] of Blind 1 to reach MY position (closed to MY)
 * knx.paramInt(24)      - Runtime [s] of Blind 2 to reach MY position (closed to MY)
 * knx.paramInt(28)      - Runtime [s] of Blind 3 to reach MY position (closed to MY)
 * knx.paramInt(32)      - Runtime [s] of Blind 4 to reach MY position (closed to MY) 
 * 
 */

#define PARAM_BLIND1    0
#define PARAM_BLIND2    PARAM_BLIND1 + 4
#define PARAM_BLIND3    PARAM_BLIND2 + 4
#define PARAM_BLIND4    PARAM_BLIND3 + 4
#define PARAM_WINDOW    PARAM_BLIND4 + 4
#define PARAM_BLIND_MY1 PARAM_WINDOW + 4
#define PARAM_BLIND_MY2 PARAM_BLIND_MY1 + 4
#define PARAM_BLIND_MY3 PARAM_BLIND_MY2 + 4
#define PARAM_BLIND_MY4 PARAM_BLIND_MY3 + 4



// Group Object starting references
#define BLIND_NUM_GO        4
#define WINDOW_NUM_GO       3
#define BLIND_ALL_NUM_GO    3
#define GO_BLIND1   1
#define GO_BLIND2   GO_BLIND1 + BLIND_NUM_GO
#define GO_BLIND3   GO_BLIND2 + BLIND_NUM_GO
#define GO_BLIND4   GO_BLIND3 + BLIND_NUM_GO

// BLINDALL only has three group objects for STOP, MY, UP-DOWN
#define GO_WINDOW1  GO_BLIND4 + BLIND_NUM_GO
#define GO_WINDOW2  GO_WINDOW1 + WINDOW_NUM_GO
#define GO_WINDOW3  GO_WINDOW2 + WINDOW_NUM_GO
#define GO_WINDOW4  GO_WINDOW3 + WINDOW_NUM_GO

#define GO_BLINDALL GO_WINDOW4 + WINDOW_NUM_GO

#define GO_RAIN     GO_BLINDALL + BLIND_ALL_NUM_GO


// Device bags for event processing recurring events
std::vector<IKNXDevice*> devices;
std::vector<IRainHandler*> rainHandlers; 


/**
 * @brief Handle and dispatch rain events
 * 
 * @param o 
 */
void onRain(GroupObject& o)
{
    uint8_t val = o.value(); 
    bool rain = (val == 1);
    
    for (auto const& e : rainHandlers) {
        e->HandleRain(rain);
    }
    
}


  
/**
 * @brief Create a Com Objects object
 * 
 */
void createComObjects()
{
    SomfyBlind* blind1 = NULL;
    SomfyBlind* blind2 = NULL;
    SomfyBlind* blind3 = NULL;
    SomfyBlind* blind4 = NULL;

    SomfyBlindAll* blindAll = NULL;

    VeluxWindow* window1 = NULL;
    VeluxWindow* window2 = NULL;
    // Can also be used to control sun screens attached to Velux windows. Same remote control.
    VeluxWindow* window3 = NULL;
    VeluxWindow* window4 = NULL;
    
    // create rain dispatcher
    GroupObject& rain = knx.getGroupObject(GO_RAIN);
    rain.dataPointType(DPT_Alarm);
    rain.callback(onRain);

  
    SomfyRTSRemoteControl* somfyRemote = SomfyRTSRemoteControl::CreateDevice("Somfy 4CH Remote", 
                                                                        SOMFY_UP,
                                                                        SOMFY_DOWN,
                                                                        SOMFY_STOPMY, 
                                                                        SOMFY_SWITCH, 
                                                                        SOMFY_CH1, 
                                                                        SOMFY_CH2, 
                                                                        SOMFY_CH3, 
                                                                        SOMFY_CH4 );
    
    DBGLN(somfyRemote->ToString().c_str());
    
    blind1 = new SomfyBlind(    "Somfy Büro",
                                somfyRemote,
                                SOMFY_CH1,                       // Channel 1
                                knx.getGroupObject(GO_BLIND1),  // Stop
                                knx.getGroupObject(GO_BLIND1+1),  //  My
                                knx.getGroupObject(GO_BLIND1+2),  // Up/Down
                                knx.getGroupObject(GO_BLIND1+3),  // position
                                knx.paramInt(PARAM_BLIND1),         
                                knx.paramInt(PARAM_BLIND_MY1)       
                            );

    devices.push_back(blind1);

    DBGLN(blind1->ToString().c_str());
    

    blind2 = new SomfyBlind(    "Somfy Tom",
                                somfyRemote,
                                SOMFY_CH2,              // Channel 2
                                knx.getGroupObject(GO_BLIND2),  // Stop
                                knx.getGroupObject(GO_BLIND2+1),  //  My
                                knx.getGroupObject(GO_BLIND2+2),  // Up/Down
                                knx.getGroupObject(GO_BLIND2+3),  // position
                                knx.paramInt(PARAM_BLIND2),         
                                knx.paramInt(PARAM_BLIND_MY2)
                            );


    DBGLN(blind2->ToString().c_str());
    
    devices.push_back(blind2);

    blind3 = new SomfyBlind(    "Somfy Blind #3",
                                somfyRemote,
                                SOMFY_CH3,              // Channel 3
                                knx.getGroupObject(GO_BLIND3),  // Stop
                                knx.getGroupObject(GO_BLIND3+1),  //  My
                                knx.getGroupObject(GO_BLIND3+2),  // Up/Down
                                knx.getGroupObject(GO_BLIND3+3),  // position
                                knx.paramInt(PARAM_BLIND3),         
                                knx.paramInt(PARAM_BLIND_MY3)
                            );

    DBGLN(blind3->ToString().c_str());

    devices.push_back(blind3);

    blind4 = new SomfyBlind(    "Somfy Blind #4",
                                somfyRemote,
                                SOMFY_CH4,              // Channel 4
                                knx.getGroupObject(GO_BLIND4),  // Stop
                                knx.getGroupObject(GO_BLIND4+1),  //  My
                                knx.getGroupObject(GO_BLIND4+2),  // Up/Down
                                knx.getGroupObject(GO_BLIND4+3),  // position
                                knx.paramInt(PARAM_BLIND4),         
                                knx.paramInt(PARAM_BLIND_MY4)
                            );

    
    DBGLN(blind4->ToString().c_str());

    devices.push_back(blind4);
    
    // SomfyAll Channel handler that updates the sub-ordinate devices and triggers accurate status information
    blindAll = new SomfyBlindAll(   "Somfy Blind ALL",
                                    somfyRemote,
                                    blind1,
                                    blind2,
                                    blind3,
                                    blind4,
                                    knx.getGroupObject(GO_BLINDALL), // Stop
                                    knx.getGroupObject(GO_BLINDALL+1), // My
                                    knx.getGroupObject(GO_BLINDALL+2) // Up/Down
                                    );

    DBGLN(blindAll->ToString().c_str()); 
    devices.push_back(blindAll);

    
    IVeluxControl* veluxRemote1 = VeluxControlRemoteIO::CreateDevice("Velux 1CH Remote #1", VELUX1_UP, VELUX1_DOWN, VELUX1_STOP);

    DBGLN(veluxRemote1->ToString().c_str());

    window1 = new VeluxWindow("Badfenster", veluxRemote1,
                                     knx.getGroupObject(GO_WINDOW1),     // stop 
                                     knx.getGroupObject(GO_WINDOW1+1),   // updown  
                                     knx.getGroupObject(GO_WINDOW1+2)             // contact
                                     );


    DBGLN(window1->ToString().c_str());
    devices.push_back(window1);
    rainHandlers.push_back(window1);

    IVeluxControl* veluxRemote2 = VeluxControlRemoteIO::CreateDevice("Velux 1CH Remote #2", VELUX2_UP, VELUX2_DOWN, VELUX2_STOP);
    DBGLN(veluxRemote2->ToString().c_str());
    
    window2 = new VeluxWindow("Velux Window #2", veluxRemote2,
                                     knx.getGroupObject(GO_WINDOW2),     // stop 
                                     knx.getGroupObject(GO_WINDOW2+1),   // updown  
                                     knx.getGroupObject(GO_WINDOW2+2)             // contact
                                     );


    DBGLN(window2->ToString().c_str());
    devices.push_back(window2);
    rainHandlers.push_back(window2);

    IVeluxControl* veluxRemote3 = VeluxControlRemoteIO::CreateDevice("Velux 1CH Remote #3", VELUX3_UP, VELUX3_DOWN, VELUX3_STOP);

    DBGLN(veluxRemote3->ToString().c_str());
    
    window3 = new VeluxWindow("Velux Window #3", veluxRemote3,
                                     knx.getGroupObject(GO_WINDOW3),     // stop 
                                     knx.getGroupObject(GO_WINDOW3+1),   // updown  
                                     knx.getGroupObject(GO_WINDOW3+2)             // contact
                                     );


    DBGLN(window3->ToString().c_str());
    devices.push_back(window3);
    rainHandlers.push_back(window3);


/* TODO: enable again as soon as WUC not needed any longer
    IVeluxControl* veluxRemote4 = VeluxControlRemoteIO::CreateDevice("Velux 1CH Remote #4", VELUX4_UP, VELUX4_DOWN, VELUX4_STOP);

    DBGLN(veluxRemote4->ToString().c_str());
    
    window4 = new VeluxWindow("Velux Window #4", veluxRemote4,
                                     knx.getGroupObject(GO_WINDOW4),     // stop 
                                     knx.getGroupObject(GO_WINDOW4+1),   // updown  
                                     knx.getGroupObject(GO_WINDOW4+2)             // contact
                                     );


    DBGLN(window4->ToString().c_str());
    rainHandlers.push_back(window4)
*/

    IVeluxControl* windowMaster = VeluxControlWindowMasterGPIO::CreateDevice("Window Master Control", VELUX4_UP, VELUX4_DOWN, knx.paramInt(PARAM_WINDOW));
    DBGLN(windowMaster->ToString().c_str());
    
    window4 = new VeluxWindow("Dachfenster", windowMaster,
                                     knx.getGroupObject(GO_WINDOW4),     // stop 
                                     knx.getGroupObject(GO_WINDOW4+1),   // updown  
                                     knx.getGroupObject(GO_WINDOW4+2)   // contact
                                     );


    DBGLN(window2->ToString().c_str());
    devices.push_back(window4);
    rainHandlers.push_back(window4);

}



/**
 * @brief 
 * 
 */
void setup()
{
    DEBUG_SERIAL.begin(9600);
    ArduinoPlatform::SerialDebug = &DEBUG_SERIAL;

    // wait 10 seconds for programming upon startup. Safety delay because we will be putting USB to sleep later
    delay(10000);

    
    
    initPowerManager();

    randomSeed(millis());

    // for debugging EEPROM issues
    /* 
    uint8_t *buf = knx.platform().getEepromBuffer(KNX_FLASH_SIZE);
    printHex("", buf, KNX_FLASH_SIZE);
    */

    DBGLN("Reading Memory");
    knx.readMemory();
    DBGLN("Memory read");

    // print values of parameters if device is already configured
    if (knx.configured())
    {
        DBGLN("Device configured.");
        createComObjects();
    }
    else
    {
        DBGLN("Device not configured.");
    }

    // pin or GPIO the programming led is connected to. Default is LED_BUILTIN
    knx.ledPin(LED_PROG);
    // is the led active on HIGH or low? Default is LOW
    knx.ledPinActiveOn(LED_PROG_ACTIVE);

    // pin or GPIO programming button is connected to A5. Default is 0, which does not work on SAMD21
    knx.buttonPin(BTN_PROG_PIN);

    
    // ensure that we can be reached if there is no address yet.
    if (knx.individualAddress() == 0)
        knx.bau().deviceObject().individualAddress(1); //65535
    // start the framework.
    
    DBGLN("Starting KNX");
    knx.start();
    DBGLN("KNX Started");

    //Serial.end();
    //SerialUSB.end();
    
}



/**
 * @brief Update internal state of devices that need to periodically send bus messages or update internal states
 * 
 */
void KNXDeviceStateUpdate()
{
    for (auto const& e : devices) {
        e->UpdateState();
    }
    
}

void loop()
{    
    // don't delay here to much. Otherwise you might lose packages or mess up the timing with ETS
    knx.loop();
    
    // only run the application code if the device was configured with ETS
    if (knx.configured())
    {
        // trigger periodic state update
        KNXDeviceStateUpdate();
    }
    
    
    // send to nap regularly so that we save power
    nap();
}