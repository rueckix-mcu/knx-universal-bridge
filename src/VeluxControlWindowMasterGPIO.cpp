

#include "VeluxControlWindowMasterGPIO.h"

VeluxControlWindowMasterGPIO* VeluxControlWindowMasterGPIO::CreateDevice(String name, pin_size_t pin_open, pin_size_t pin_close,  uint32_t runtime) {
	VeluxControlWindowMasterGPIO* obj = new VeluxControlWindowMasterGPIO();

    obj->_name = name; 
    obj->_open = pin_open;
    obj->_close = pin_close;
    obj->_stop = 0;
    
    
    obj->setupOutputPin(pin_open);
    obj->setupOutputPin(pin_close);
    obj->_timerEndposition.setInterval(runtime * 1000); // convert seconds to millis
    obj->_timerCooldown.setInterval(COOLDOWN);

    return obj;
}

void VeluxControlWindowMasterGPIO::Open() {
    
    Stop();
    cooldown(); // we only block when trying to move again
    
    // pulling the pin low, closes the relay
    pullLow(_open);
    
    _timerEndposition.reset();
    _isMoving = true;

	IVeluxControl::Open();
}

void VeluxControlWindowMasterGPIO::cooldown()
{
    // wait for cooldown timer before moving
    while (!_timerCooldown.isReady()) { delay(10); }
}


void VeluxControlWindowMasterGPIO::Close() {
  
    Stop();
    cooldown(); // we only block when trying to move again

    // pulling the pin low, closes the relay
    pullLow(_close);
    
    _timerEndposition.reset();
    _isMoving = true;

	IVeluxControl::Close();
}

void VeluxControlWindowMasterGPIO::Stop() {

    // start cooldown timer if the motor is moving
    if (_isMoving)
    {
        // Pulling high opens the relays
        pullHigh(_close);
        pullHigh(_open);
        _timerCooldown.reset(); // we don't block here
        _isMoving = false; 
    }
    
	IVeluxControl::Stop();
}

void VeluxControlWindowMasterGPIO::UpdateState()
{
    // ensure movement is stopped after we have reached an endposition
    if (_isMoving && _timerEndposition.isReady())
        Stop();

}