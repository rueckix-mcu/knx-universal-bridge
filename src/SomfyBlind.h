#pragma once


#include "KNXDevice.h"

#include <SomfyRTSRemoteControl.h>
#include <SimpleTimer.h>



class SomfyBlind: public IKNXDevice
{

    // permit access to private attributes and methods
    friend class SomfyBlindAll;

    private:
        String _name;
        SomfyRTSRemoteControl* _remote = NULL;
        SomfyChannel _channel;
        GroupObject& _stop;
        GroupObject& _my;
        GroupObject& _updown;
        GroupObject& _position;
        
        
        float _approxPosition = POSITION_UP; // assume blind is up upon init
        
        
        SimpleTimer _timerStatusUpdate;
        SimpleTimer _timerEndPosition;
        bool _isMoving = false;
        bool _isMovingUp = false;
        float _isMovingTo;


        uint32_t _paramRuntime; // runtime parameter in milliseconds
        float _positionMy; 

        float _positionEstimate; // 0 = fully down, 1= fully up
        unsigned long _lastUpdate = -1; // time in millis of last update
        static constexpr unsigned long UPDATE_PERIOD = 5000; // update every 5 seconds
        static constexpr float POSITION_UP = 1.0f;
        static constexpr float POSITION_DOWN = 0.0f;
        

        float _movementPerPeriod;


    public:
        /**
         * @brief Construct a new Somfy Blind object
         * 
         * @param name      Friendly name of the object
         * @param remoteControl RemoteControl object. WILL NOT BE DELETED IN THE d'tor        
         * @param channel   Channel on the remote control
         * @param goStop    KNX group object for stop button event          [IN]
         * @param goMy      KNX group object for my button event            [IN]
         * @param goUpDown  KNX group object for up/down button event       [IN]
         * @param goPosition    KNX group object for position (status only)    [OUT]
         * @param paramRuntime  Runtime of the blind (fully down to fully up) in seconds
         * @param paramRuntimeMy Runtime of the blind to MY position (fully down to MY) in seconds
         */
        SomfyBlind(String name, SomfyRTSRemoteControl* remoteControl, SomfyChannel channel, GroupObject& goStop, GroupObject&  goMy, GroupObject&  goUpDown, GroupObject& goPosition,   uint32_t paramRuntime, uint32_t paramRuntimeMy);
        ~SomfyBlind();
    /**
     * @brief Helper functions
     * 
     */
    public: 
        virtual String ToString();
        virtual void UpdateState();

        virtual const char* GetName() {return _name.c_str();};


    /**
     * @brief Event handler
     * 
     */
    protected:
        /**
         * @brief Handler for Stop event (KNX Group Object)
         * 
         * @param o 
         */
        void onStop(GroupObject& o);
        
        /**
         * @brief Handler for My event (KNX Group Object)
         * 
         * @param o 
         */
        void onMy(GroupObject& o);
        

        /**
         * @brief Handler for UpDown event (KNX Group Object)
         * 
         * @param o 
         */
        void onUpDown(GroupObject& o);
        

        /**
         * @brief Periodically sends current position. There is no sensor on Somfy blinds. 
         * The current position is estimated based on the the configured "runtime" parameter.
         * The position is reset upon a certain 100% closed or 100% opened event.
         * 
         */
        void statusUpdatePosition();


        /**
         * @brief Set the status to "moving to $target"
         * 
         * @param target - Target position to move to
         */
        void setMovingTo(float target);    

        /**
         * @brief Update internal state to "not moving"
         * 
         */
        void setNotMoving();

};