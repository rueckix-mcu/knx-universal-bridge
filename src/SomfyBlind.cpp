#include "SomfyBlind.h"

#include <assert.h>


// for std::function, std::bind
#include <functional>


SomfyBlind::SomfyBlind(String name, SomfyRTSRemoteControl* remoteControl, SomfyChannel channel, GroupObject& goStop, GroupObject&  goMy, GroupObject&  goUpDown, GroupObject& goPosition, uint32_t paramRuntime, uint32_t paramRuntimeMy)
        : _name(name), _remote(remoteControl), _channel(channel), _stop(goStop), _my(goMy), _updown(goUpDown), _position(goPosition)
{
    // ensure valid object
    assert(_remote);

    _stop.dataPointType(DPT_Switch);
    _my.dataPointType(DPT_Switch);
    _updown.dataPointType(DPT_UpDown);
    _position.dataPointType(DPT_Percent_U8);

    _stop.callback(std::bind(&SomfyBlind::onStop, this, std::placeholders::_1));
    _my.callback(std::bind(&SomfyBlind::onMy, this, std::placeholders::_1));
    _updown.callback(std::bind(&SomfyBlind::onUpDown, this, std::placeholders::_1));
    

    _paramRuntime = paramRuntime * 1000;
    _positionMy = (float) paramRuntimeMy / (float) paramRuntime; // returns a percentage 

    _timerStatusUpdate.setInterval(UPDATE_PERIOD);
    
    _movementPerPeriod =  (float) UPDATE_PERIOD / (float)_paramRuntime;

};

SomfyBlind::~SomfyBlind()
{
    //delete _remote;
}

String SomfyBlind::ToString()
{
    String s = "SomfyBlind:\t";
    s += _name;
    s += "\n_stop\t";
    s += (_stop.value() ? "true": "false");

    s += "\n_my\t";
    s += (_my.value() ? "true": "false");

    s += "\n_updown\t";
    s += ((bool)_updown.value() > 0 ? "Up": "Down");

    s += "\n_position\t";
    s += (uint8_t)_position.value();
    s += "%";

    s += "\n_paramRuntime\t";
    s += _paramRuntime;

    s += "\n_positionMy\t";
    s += _positionMy;
    
    return s;
};

void SomfyBlind::onStop(GroupObject& o)
{
    DBG(GetName());
    DBGLN("--SomfyBlind: Stop received");

    if (_isMoving)
    {   // stop only if assumed moving
        if (_remote->StopMy(_channel))
            setNotMoving();
    }

    // do nothing if already stopped
};

void SomfyBlind::onMy(GroupObject& o)
{
    DBG(GetName());
    DBGLN("--SomfyBlind: My command received");
    
    
    if (_isMoving)
    {  // assume we are moving and need to stop first

        // stop movement
        if (_remote->StopMy(_channel))
            setNotMoving();
    }

    // trigger movement to My position
    
    DBGLN("\tMoving to MY position");
    
    // _isMoving remains true;
    if (_remote->StopMy(_channel))
        setMovingTo(_positionMy);
};

void SomfyBlind::onUpDown(GroupObject& o)
{
    DBG(GetName());
    DBGLN("--SomfyBlind: UpDown received");
    
    bool val = o.value(); 
    float target;
    bool success = false;

    // 0 == UP
    if (val == 0)
    {
        success = _remote->Up(_channel);
        target = POSITION_UP;
        DBGLN("\tUP command");
    }
    else   
    {
        success = _remote->Down(_channel);
        target = POSITION_DOWN;
        DBGLN("\tDOWN command");
    }

    if (success)
        setMovingTo(target);    
};

void SomfyBlind::setMovingTo(float target)
{
    _isMoving = true;
    _isMovingTo = target;
    
    // determine movement direction
    if (_approxPosition < target)
    {
        _isMovingUp = true;
    }
    else   
    {
        _isMovingUp = false;
    }


    float t = _paramRuntime * fabs(target - _approxPosition);
    _timerEndPosition.setInterval(t);
    DBG(GetName());
    DBGLN("--SomfyBlind: Moving from approximate position");
    DBG("\tRuntime: ");
    DBGLN(t);


    // set timers based on approximate position
    _timerEndPosition.reset();
    _timerStatusUpdate.reset();
};

void SomfyBlind::setNotMoving()
{
    _isMoving = false;
};


void SomfyBlind::statusUpdatePosition()
{
    DBG(GetName());
    DBGLN("--SomfyBlind: Sending updated position estimate");
    
    // map to [0, 255] interval
    uint8_t val = _approxPosition * UINT8_MAX;
    _position.value(val);
};


void SomfyBlind::UpdateState()
{
    _remote->UpdateState();
    
    // enable only for debugging: 
    //DBGLN("SomfyBlind: Periodically updating internal state");

    if (_isMoving)
    {            
        // capture end of movement
        if (_timerEndPosition.isReady())
        {
            setNotMoving();
            _approxPosition = _isMovingTo;

            DBG(GetName());
            DBGLN("--SomfyBlind: Periodically updating internal state");
            DBGLN("\tInferring end of movement");

            // report end of movement and skip the update branch below
            statusUpdatePosition();
        } 
        // approximate intermediate position
        else if (_timerStatusUpdate.isReady())
        {
            _timerStatusUpdate.reset();
            _approxPosition += (_isMovingUp ? 1:-1) * _movementPerPeriod;

            DBG(GetName());
            DBGLN("--SomfyBlind: Periodically updating internal state");
            DBG("\tUpdating approximate position: ");
            DBGLN(_approxPosition);

        
            // ensure we are are staying within the [0,1] interval       
            if (_approxPosition >= POSITION_UP)
            {
                _approxPosition = POSITION_UP;
            }
            else if (_approxPosition <= POSITION_DOWN)
            {
                _approxPosition = POSITION_DOWN;
            }

            statusUpdatePosition();
        }        
    }
    
};