#pragma once


#ifdef DEBUGPRINT
#define DBG(X)      print(X)
#define DBGLN(X)    println(X)  
#else
#define DBG(X)
#define DBGLN(X)
#endif