#include <knx.h>
#include "VeluxWindow.h"

#include <assert.h>



VeluxWindow::VeluxWindow(String name, IVeluxControl* remoteControl, GroupObject& goStop, GroupObject&  goUpDown, GroupObject& goContact)
	: _name(name), _remote(remoteControl), _stop(goStop), _updown(goUpDown), _contact(goContact)
{
    // ensure valid object
    assert(_remote);
    _stop.dataPointType(DPT_Switch);
    _updown.dataPointType(DPT_OpenClose);

    _contact.dataPointType(DPT_Window_Door);

    _stop.callback(std::bind(&VeluxWindow::onStop, this, std::placeholders::_1));
    _updown.callback(std::bind(&VeluxWindow::onOpenClose, this, std::placeholders::_1));
    _contact.callback(std::bind(&VeluxWindow::onContact, this, std::placeholders::_1));
}


VeluxWindow::~VeluxWindow() {
	//delete _remote;
}

void VeluxWindow::UpdateState()
{
    _remote->UpdateState();
}

String VeluxWindow::ToString() {
    String s = "VeluxWindow:\t";
    s += _name;
    s += "\n_stop\t";
    s += (_stop.value() ? "true": "false");

    s += "\n_updown\t";
    s += ((bool)_updown.value() > 0 ? "Up": "Down");

    s += "\n_contact\t";
    s += (_contact.value() ? "true": "false");

    s += "\n_isOpen\t";
    s += (_isOpen ? "true": "false");

    
    return s;	
}

void VeluxWindow::onStop(GroupObject& o) {
    DBG(GetName());
	DBGLN("--VeluxWindow: Stop received");
    _remote->Stop();
}

void VeluxWindow::onOpenClose(GroupObject& o) {
    DBG(GetName());
	DBGLN("--VeluxWindow: Open/Close received");




    uint8_t val = o.value(); 
    
    
    if (val == 1) // val == 1 means close, val == 0 means open
    {
        _remote->Close();
        DBGLN("\tCLOSE command");
    }
    else   
    {
        if (_isRaining)
        {
            DBGLN("\t Raining. No Action.");
        }
        else
        {
            _remote->Open();
            DBGLN("\tOPEN command");
        }
    }

}

void VeluxWindow::onContact(GroupObject& o) {
    DBG(GetName());
	DBGLN("--VeluxWindow: Contact event received");

    // contact. ==0 (CLOSED) / ==1 (OPEN)
    uint8_t val = o.value(); 
    
    if (val == 0)
    {
        _isOpen = false;
        DBGLN("\tCLOSED");
    }
    else   
    {
        _isOpen = true;
        DBGLN("\tOPEN");
    }
}

void VeluxWindow::HandleRain(bool isRaining) {
	DBG(GetName());
    DBGLN("--VeluxWindow: Rain event received.");

    _isRaining = isRaining;

    if (_isOpen && isRaining)
    {
        DBGLN("\tWindow Open upon rain alarm: Closing.");
        _remote->Close();
    }
}
