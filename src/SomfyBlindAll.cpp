
#include "SomfyBlindAll.h"
#include <assert.h>



SomfyBlindAll::SomfyBlindAll(String name, SomfyRTSRemoteControl* remote, SomfyBlind* blind1, SomfyBlind* blind2, SomfyBlind* blind3, SomfyBlind* blind4, GroupObject& goStop, GroupObject&  goMy, GroupObject&  goUpDown) 
        : _name(name), _remote(remote), _blind1(blind1), _blind2(blind2), _blind3(blind3), _blind4(blind4), _stop(goStop), _my(goMy), _updown(goUpDown)
{
    // ensure valid object
    assert(remote && blind1 && blind2 && blind3 && blind4);

    _stop.dataPointType(DPT_Switch);
    _my.dataPointType(DPT_Switch);
    _updown.dataPointType(DPT_UpDown);
    
    _stop.callback(std::bind(&SomfyBlindAll::onStop, this, std::placeholders::_1));
    _my.callback(std::bind(&SomfyBlindAll::onMy, this, std::placeholders::_1));
    _updown.callback(std::bind(&SomfyBlindAll::onUpDown, this, std::placeholders::_1));
    	
}

SomfyBlindAll::~SomfyBlindAll() {

    //delete _remote;
    //delete _blind1;
    //delete _blind2;
    //delete _blind3;
    //delete _blind4;
    
}

String SomfyBlindAll::ToString() {
	String s = "SomfyBlindAll:\t";
    s += _name;
    s += "\n_stop\t";
    s += (_stop.value() ? "true": "false");

    s += "\n_my\t";
    s += (_my.value() ? "true": "false");

    s += "\n_updown\t";
    s += ((bool)_updown.value() > 0 ? "Up": "Down");
    
    return s;
}

void SomfyBlindAll::onStop(GroupObject& o) {
    DBGLN("SomfyBlindAll: Stop received");
    
    if (_remote->StopMy(SomfyRTSRemoteControl::CHANNEL_ALL))
    { 
        _blind1->setNotMoving();
        _blind2->setNotMoving();
        _blind3->setNotMoving();
        _blind4->setNotMoving();
    }
}

void SomfyBlindAll::onMy(GroupObject& o) {
	DBGLN("SomfyBlindAll: My command received");
    
    
    // remains: _isMoving = true;
    if (_remote->StopMy(SomfyRTSRemoteControl::CHANNEL_ALL))
    {
        _blind1->setMovingTo(_blind1->_positionMy);
        _blind2->setMovingTo(_blind2->_positionMy);
        _blind3->setMovingTo(_blind3->_positionMy);
        _blind4->setMovingTo(_blind4->_positionMy);
    }
        
}

void SomfyBlindAll::onUpDown(GroupObject& o) {
	DBGLN("SomfyBlindAll: UpDown received");


    bool val = o.value(); 
    float target;
    bool success = false;

    // 0 == UP
    if (val == 0)
    {
        success = _remote->Up(SomfyRTSRemoteControl::CHANNEL_ALL);
        target = SomfyBlind::POSITION_UP;
        DBGLN("\tUP command");
    }
    else   
    {
        success = _remote->Down(SomfyRTSRemoteControl::CHANNEL_ALL);
        target = SomfyBlind::POSITION_DOWN;
        DBGLN("\tDOWN command");
    }

    if (success)
    {
        _blind1->setMovingTo(target);    
        _blind2->setMovingTo(target);    
        _blind3->setMovingTo(target);    
        _blind4->setMovingTo(target);    
    }
}
