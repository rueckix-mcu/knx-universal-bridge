#pragma once

#include "platform.h"


#include <WString.h>
#include <knx.h>


class IKNXDeviceInterface
{
    public:
        virtual ~IKNXDeviceInterface() {};
        
    protected: 
        IKNXDeviceInterface() { };
        
        
        
        /**
         * @brief Trigger a phyical button press
         * 
         * @param pin - Pin connected to the button [active low]
         */
        void triggerButton(pin_size_t pin)
        {
            digitalWrite(pin, LOW); 
            delay(100); // verified with SOMFY RTS and Velux IO
            digitalWrite(pin, HIGH); 
        };

        /**
         * @brief Declare pin to be an output pin
         * 
         * @param pin 
         */
        void setupOutputPin(pin_size_t pin)
        {
            pinMode(pin, OUTPUT);
            digitalWrite(pin, HIGH);
        };


        /**
         * @brief Declare pin to be an input pin with internal pullup
         * 
         * @param pin 
         */
        void setupInputPin(pin_size_t pin)
        {
            pinMode(pin, INPUT_PULLUP); 
        };


        /**
         * @brief Pull the pin to GROUND
         * 
         * @param pin 
         */
        void pullLow(pin_size_t pin)
        {
            digitalWrite(pin, LOW);
        };

        /**
         * @brief Pull the pin to VCC
         * 
         * @param pin 
         */
        void pullHigh(pin_size_t pin)
        {
            digitalWrite(pin, HIGH);
        };

        /**
         * @brief Read (digital) pin state connected to LED. If the level is LOW, the LED is on.
         * 
         * @param pin 
         * @return true - if the LED is on
         * @return false  - if the LED is off
         */
        bool readLEDState(pin_size_t pin)
        {
            return (digitalRead(pin) == LOW ? true: false);
        };

        

    public: 
        /**
         * 
         * @brief Implementing classes need to provide a factor function to create a Device object 
         * 
         * @return IKNXDeviceInterface* 
         
        static IKNXDeviceInterface* CreateDevice() = 0;
        */

    public:
        /**
         * @brief Pretty print the object
         * 
         * @return String 
         */
        virtual String ToString() = 0;

        /**
         * @brief Return the device type name
         * 
         */
        virtual String GetDeviceType() = 0; 


        /**
         * @brief Update internal state variables (typically based on timers)
         * 
         */
        virtual void UpdateState() {/*nothing*/};

    



};