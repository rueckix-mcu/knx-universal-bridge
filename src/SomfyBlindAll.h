#pragma once

#include "KNXDevice.h"

#include <SomfyRTSRemoteControl.h>
#include <SomfyBlind.h>

class SomfyBlindAll: public IKNXDevice
{
    private:
        String _name;
        SomfyRTSRemoteControl* _remote = NULL;
        SomfyBlind* _blind1;
        SomfyBlind* _blind2;
        SomfyBlind* _blind3;
        SomfyBlind* _blind4;

        GroupObject& _stop;
        GroupObject& _my;
        GroupObject& _updown;
       

    public:


        /**
         * @brief Construct a new Somfy All-Blind object, handling all blinds associated with a 4CH remote
         * 
         * @param name      Friendly name of the object
         * @param remoteControl RemoteControl object. WILL NOT BE DELETED IN THE d'tor                
         * @param blind1    Blind 1 reference. WILL NOT BE DELETED IN THE d'tor                  
         * @param blind2    Blind 2 reference. WILL NOT BE DELETED IN THE d'tor                  
         * @param blind3    Blind 3 reference. WILL NOT BE DELETED IN THE d'tor                  
         * @param blind4    Blind 4 reference. WILL NOT BE DELETED IN THE d'tor                  
         * @param goStop    KNX group object for stop button event          [IN]
         * @param goMy      KNX group object for my button event            [IN]
         * @param goUpDown  KNX group object for up/down button event       [IN]
         */
        SomfyBlindAll(String name, SomfyRTSRemoteControl* remote, SomfyBlind* blind1, SomfyBlind* blind2, SomfyBlind* blind3, SomfyBlind* blind4, GroupObject& goStop, GroupObject&  goMy, GroupObject&  goUpDown);
        ~SomfyBlindAll();
    /**
     * @brief Helper functions
     * 
     */
    public: 
        virtual String ToString();

        virtual const char* GetName() {return _name.c_str();};


    /**
     * @brief Event handler
     * 
     */
    protected:
        /**
         * @brief Handler for Stop event (KNX Group Object)
         * 
         * @param o 
         */
        void onStop(GroupObject& o);
        
        /**
         * @brief Handler for My event (KNX Group Object)
         * 
         * @param o 
         */
        void onMy(GroupObject& o);
        

        /**
         * @brief Handler for UpDown event (KNX Group Object)
         * 
         * @param o 
         */
        void onUpDown(GroupObject& o);
        

   
};