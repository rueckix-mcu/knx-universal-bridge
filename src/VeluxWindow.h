#pragma once


#include <knx.h>
#include "VeluxControl.h"
#include "KNXDevice.h"
#include "RainHandler.h"


class VeluxWindow: public IKNXDevice, public IRainHandler
{
private:
        String _name;
        IVeluxControl* _remote = NULL;
        GroupObject& _stop;
        GroupObject& _updown;
        GroupObject& _contact;
        bool _isOpen = true;
        bool _isRaining = false;
           

public:
    /**
     * @brief Construct a new Velux Window object
     * 
     * @param name           Friendly name of the object       
     * @param remoteControl RemoteControl object. WILL NOT BE DELETED IN THE d'tor
     * @param goStop            KNX group object for stop button event          [IN]
     * @param goUpDown          KNX group object for up/down button event       [IN]
     * @param goContact         KNX group object for the window contact (reed contact)   [IN]
     */
    VeluxWindow(String name, IVeluxControl* remoteControl, GroupObject& goStop, GroupObject&  goUpDown, GroupObject& goContact);
    ~VeluxWindow();


    /**
     * @brief Helper functions
     * 
     */
    public: 
        /**
         * @brief Pretty-print of the object
         * 
         * @return String 
         */
        virtual String ToString();

        virtual const char* GetName() {return _name.c_str();};

        virtual void UpdateState();

       /**
     * @brief Event handler
     * 
     */
    protected:
        /**
         * @brief Handler for Stop event (KNX Group Object)
         * 
         * @param o 
         */
        void onStop(GroupObject& o);
        
        

        /**
         * @brief Handler for Open/Close event (KNX Group Object)
         * 
         * @param o 
         */
        void onOpenClose(GroupObject& o);
        

        /**
         * @brief Handler for Contact (is open/is closed) event (KNX Group Object)
         * 
         * @param o 
         */
        void onContact(GroupObject& o);


    public: 
        virtual void HandleRain(bool isRaining);

}; 
