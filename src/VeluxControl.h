#pragma once

#include <KNXDeviceInterface.h>

class IVeluxControl: public IKNXDeviceInterface
{
protected:
    String _name;
    pin_size_t _open;
    pin_size_t _close;
    pin_size_t _stop;


public:
        
        virtual String GetDeviceType() = 0;

        virtual String ToString(){
            String s = GetDeviceType();
            s += ":\t";
            s += _name;
            s += "\nOutput PINs:\t OPEN=";
            s += _open;
            s += "\tCLOSE=";
            s += _close;
            s += "\tSTOP=";
            s += _stop; 
            
            return s;
        };

public:
        /**
         * @brief Trigger the Open button
         * 
         */
        virtual void Open()
        {
            DBG(GetDeviceType().c_str());
            DBGLN(": Trigger OPEN");	
        };
        
        /**
         * @brief Trigger the Close butotn
         * 
         */
        virtual void Close() 
        {
            DBG(GetDeviceType().c_str());
            DBGLN(": Trigger CLOSE");	
        };

        /**
         * @brief Trigger the Stop button
         * 
         */
        virtual void Stop()
         {
            DBG(GetDeviceType().c_str());
            DBGLN(": Trigger STOP");	
        };

};

