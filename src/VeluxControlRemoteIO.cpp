
#include "VeluxControlRemoteIO.h"

VeluxControlRemoteIO* VeluxControlRemoteIO::CreateDevice(String name, pin_size_t pin_open, pin_size_t pin_close, pin_size_t pin_stop) {
    VeluxControlRemoteIO* obj = new VeluxControlRemoteIO();

    obj->_name = name; 
    obj->_open = pin_open;
    obj->_close = pin_close;
    obj->_stop = pin_stop;
    
    
    obj->setupOutputPin(pin_open);
    obj->setupOutputPin(pin_close);
    obj->setupOutputPin(pin_stop);
    
    return obj;
}


void VeluxControlRemoteIO::Open() {
    triggerButton(_open);

    IVeluxControl::Open();
}

void VeluxControlRemoteIO::Close() {
	triggerButton(_close);
    
    
    IVeluxControl::Close();
}

void VeluxControlRemoteIO::Stop() {
	triggerButton(_stop);
    
    IVeluxControl::Stop();
}
