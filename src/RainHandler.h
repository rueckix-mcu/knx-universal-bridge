#pragma once

#include "platform.h"


class IRainHandler
{ 
    public:
        /**
         * @brief Handle a rain event
         * 
         * @param isRaining     - true if rain is detected, false otherwise
         */
        virtual void HandleRain(bool isRaining) = 0;
};