#include "platform.h"

void initPowerManager()
{
    #ifdef ARDUINO_ARCH_SAMD
        zpmRTCInit();
    #endif
    
    #ifdef ARDUINO_ARCH_STM32
        
    #endif

}

void nap()
{
    #ifdef ARDUINO_ARCH_SAMD
        uint32_t now = zpmRTCGetClock();
        zpmRTCInterruptAt(now + 1, NULL);
        zpmSleep(); // kills the USB interface, not good for debugging
        //zpmPlayPossum(); // keeps USB alive, good for debugging
    #endif

    #ifdef ARDUINO_ARCH_STM32
        // cheap sleep function for power saving
        // The LowPower library is not working properly until STM32 Arduino Core is updated beyond 1.9.0
        __WFI();
        /*
        uint32_t start = getCurrentMillis();
        do {
        __WFI();
        } while (getCurrentMillis() - start < STM32_NAP_TIME);
        */
    #endif
}
