#pragma once

#include "VeluxControl.h"
#include <SimpleTimer.h>


/**
 * @brief Controlling a WUC 100 0102 window motor driver.
 * 
 * It has a three pin header.
 * 
 * Pulling them down signals UP or DOWN. 
 * A timer is used to pull both signals to VCC after a configurable time @param runtime
 * Cooldown is recommended when changing direction.
 * 
 * 
 * ####################
 * | UP  | DOWN  | GND|
 * ####################
 *
 * 
 * ATTN: A relay must be used to isolate the WUC from the microcontroller as the 3 pin connector uses 24V.
 *  
 * 
 */

class VeluxControlWindowMasterGPIO: public IVeluxControl
{
private:
    uint32_t _runtime;
    SimpleTimer _timerCooldown;
    SimpleTimer _timerEndposition;
    static constexpr uint32_t COOLDOWN = 500; // 500ms cooldown between up and down commands
    bool _isMoving = false;
    

protected:
    /**
     * @brief wait for cooldown timer to elapse (blocking)
     * 
     */
    void cooldown();

public:

    /**
     * @brief Create a Window Master control object that interfaces with Window Master WUC 100 via GPIO ports
     * 
     * @param name      - friendly name
     * @param pin_open  - open pin
     * @param pin_close   - close pin
     * @param runtime   - expected runtime (close to open) in seconds
     * @return VeluxControlWindowMasterGPIO* 
     */
    static VeluxControlWindowMasterGPIO* CreateDevice(String name, pin_size_t pin_open, pin_size_t pin_close,  uint32_t runtime);
    
    virtual String GetDeviceType() { return "VeluxControlGPIO"; };


    virtual void UpdateState();

    /**
     * @brief Trigger the Open button
     * 
     */
    virtual void Open();
    
    /**
     * @brief Trigger the Close butotn
     * 
     */
    virtual void Close();

    /**
     * @brief Trigger the Stop button
     * 
     */
    virtual void Stop();
};

