
#pragma once


/**
 * @file platform.h
 * @author rueckix
 * @brief Contains all platform dependent code
 * @version 0.1
 * @date 2021-01-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */



#include <Arduino.h>


#include "debug.h"

#pragma once
#ifdef ARDUINO_ARCH_STM32

    #define pin_size_t uint32_t

    #define DEBUG_SERIAL Serial

    // time to take sleep nap [ms]
    // #define STM32_NAP_TIME 1 

    // PINS on STM32
    #define BTN_PROG_PIN USER_BTN
    #define LED_PROG LED_BUILTIN //=D13
    #define LED_PROG_ACTIVE LOW


    // 5V Tolerance on WeAct Blackpill V2
    // All pins except PA0 [A0] and PB5 [B5]
    // No isolator (e.g., ADUM) needed for connecting to Siemens BCU, unless we required more than 20-50mA. Otherwise, fast rate isolator is required

    // Debug Serial Port Serial=Serial1
    // PIN_SERIAL_RX           PA10
    // PIN_SERIAL_TX           PA9

    
    // KNX UART uses Serial2
    // UART RX                  PA3 // Connected to ADUM VOB
    // UART TX                  PA2 // Connected to ADUM VIA

    // PINS verified: OK
    #define SOMFY_UP        PB4
    #define SOMFY_STOPMY    PB3
    #define SOMFY_DOWN      PA15
    #define SOMFY_SWITCH    PA12
    // CONFLICT with JTAG is resolved by setting pin mode (disabled JTAG)

    #define SOMFY_CH4       PB12
    #define SOMFY_CH3       PB13
    #define SOMFY_CH2       PB14
    #define SOMFY_CH1       PB15


    // PINS verified: OK
    #define VELUX1_UP       PB10
    #define VELUX1_DOWN     PB2
    #define VELUX1_STOP     PB1
    
    // PINS verified: OK
    #define VELUX2_UP       PA7
    #define VELUX2_DOWN     PA6
    #define VELUX2_STOP     PA5   
    
    // PINS verified: OK
    #define VELUX3_UP      PC15     
    #define VELUX3_DOWN    PC14
    #define VELUX3_STOP    PC13     // ATTN: shared with onboard LED, will get triggered on progmode

    // PINS verified: OK
    #define VELUX4_UP       PB8
    #define VELUX4_DOWN     PB7
    #define VELUX4_STOP     PB6

#endif

#ifdef ARDUINO_ARCH_SAMD
// For testing only. Does not offer enought IO
    #include <ZeroPowerManager.h>

    #define DEBUG_SERIAL SerialUSB
    

    // PINS on SAMD21
    // ATTN: Digital 2 does not appear to work with interrupts
    #define BTN_PROG_PIN A5
    #define LED_PROG LED_BUILTIN //=D13
    #define LED_PROG_ACTIVE HIGH

    // D0 - UART RX
    // D1 - UART TX

    #define SOMFY_UP    2
    #define SOMFY_DOWN  3
    #define SOMFY_STOPMY  4
    #define SOMFY_SWITCH 5
    #define SOMFY_CH1   A0
    #define SOMFY_CH2   A1
    #define SOMFY_CH3   A2
    #define SOMFY_CH4   A3


    #define VELUX1_UP       6
    #define VELUX1_DOWN     7
    #define VELUX1_STOP     8

    #define VELUX2_UP       9
    #define VELUX2_DOWN     21
    #define VELUX2_STOP     22

    #define VELUX1_SUN_UP       
    #define VELUX1_SUN_DOWN     
    #define VELUX1_SUN_STOP     

    #define VELUX2_SUN_UP       
    #define VELUX2_SUN_DOWN     
    #define VELUX2_SUN_STOP    

#endif


/**
 * @brief Initialize power/sleep manager
 * 
 */
void initPowerManager();
/**
 * @brief Function invokes deep sleep followed by an interrupt after approximately 1 ms. Significantly reduces the power draw from 15mA to 3mA at the expense of a 1ms delay.
 * 
 */
void nap();